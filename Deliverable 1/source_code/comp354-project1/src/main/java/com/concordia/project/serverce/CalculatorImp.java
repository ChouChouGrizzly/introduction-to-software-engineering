package com.concoridia.project.serverce;

import com.concoridia.project.function.Calculation;
import org.springframework.context.annotation.Configuration;

import java.util.Queue;

/**
 * @author Fred Zhang
 * @create 2020-05-11 11:14 AM
 */
//It is a class to implement the desired functions
//We consider it a javaBean and put it to IOC
@Configuration
public class CalculatorImp implements Calculation {

    public static final double E = 2.7182818284590452354;

    public static final double PI = 3.14159265358979323846;

    private static final double LOG10 = 2.30258509299404568402;

    @Override
    public double sin(double x)
    {
        double sin = 0;

        int sign;
        x %= 2 * PI;
        if (x > -2 * PI && x < -3.0 * PI / 2)
        {
            x += 2 * PI;
            sign = 1;
        }
        else if (x >= -3.0 * PI / 2 && x < - PI / 2)
        {
            x += PI;
            sign = -1;
        }
        else if (x > PI / 2 && x <= 3 * PI / 2)
        {
            x -= PI;
            sign = -1;
        }
        else if (x > 3 * PI / 2 && x < 2 * PI)
        {
            x -= 2 * PI;
            sign = 1;
        }
        else
            sign = 1;

        double numerator = x;
        double denominator = 1;
        for (int i = 0; i < 11; i++)
        {
            double term = sign * numerator / denominator;
            sin += term;

            sign = -sign;
            numerator *= x * x;
            denominator *= (2 * i + 2) * (2 * i + 3);
        }

        return sin;
    }

    @Override
    public double exp10(double x)
    {
        return exp(x * LOG10);
    }

    @Override
    public double log(double x)
    {
        if (x <= 0)
            throw new ArithmeticException();

        double log = 0;

        if (x < 2.0/11)
        {
            while (x < 2.0/11)
            {
                x *= 10;
                log -= LOG10;
            }
        }
        else if (x > 20.0/11)
        {
            while (x > 20.0/11)
            {
                x /= 10;
                log += LOG10;
            }
        }

        return log + log1p(x - 1);
    }

    @Override
    public double exp(double x)
    {
        double exp = 0;

        double numerator = 1;
        double denominator = 1;
        for (int i = 0; i < 130; i++)
        {
            double term = numerator / denominator;
            exp += term;

            numerator *= x;
            denominator *= i + 1;
        }

        return exp;
    }

    @Override
    public double sinh(double x)
    {
        double sinh = 0;

        double numerator = x;
        double denominator = 1;
        for (int i = 0; i < 66; i++)
        {
            double term = numerator / denominator;
            sinh += term;

            numerator *= x * x;
            denominator *= (2 * i + 2) * (2 * i + 3);
        }

        return sinh;
    }


    @Override
    public double pow(double x, double y)
    {
        if (isInt(y))
            return round(pow2(x, (int)Math.round(y)));

        return round(exp(y * log(x)));
    }
    public boolean isInt(double num)
    {
        return num > Math.round(num) - 0.000001 && num < Math.round(num) + 0.000001;
    }

    public double round(double num)
    {
        if (isInt(num))
            return Math.round(num);
        else
            return num;
    }
    private double pow2(double x, int y)
    {
        double pow = 1;

        if (y < 0)
        {
            x = 1 / x;
            y = -y;
        }

        for (; y != 0; x *= x, y /= 2)
        {
            if (y % 2 == 1)
                pow *= x;
        }

        return round(pow);
    }
    public boolean isFakeDouble(double num){

        return num == Math.round(num);
    }

    @Override
    public double add(double x, double y) {
        return x + y;
    }


    @Override
    public double sub(double x, double y) {
        return x - y;
    }


    @Override
    public double mul(double x, double y) {
        return x * y;
    }

    @Override
    public double div(double x, double y) {
        if(y == 0) throw new IllegalArgumentException("divisor is 0");
        return x / y;
    }

    private double log1p(double x)
    {
        double log1p = 0;

        int sign = 1;
        double numerator = x;
        double denominator = 1;
        for (int i = 1; i < 159; i++)
        {
            double term = sign * numerator / denominator;
            log1p += term;

            sign = -sign;
            numerator *= x;
            denominator += 1;
        }

        return log1p;
    }



    public static void main(String[] args) {
        CalculatorImp calculatorImp = new CalculatorImp();
        System.out.println(calculatorImp.sin(3.1415926));
        System.out.println(Math.sin(3.1415926));
        System.out.println(calculatorImp.pow(4, -2.5));
        System.out.println(Math.pow(4, -2.5));
        System.out.println(calculatorImp.log1p(18));
        System.out.println(Math.log(18));


    }
}

